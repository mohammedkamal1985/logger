﻿namespace Logging.Infrastructure.Entity
{
    public class Target
    {
        public string Type { get; set; }
        public string Server { get; set; }
        public string ConnectionString { get; set; }
        public string MinimumLogLevel { get; set; }
    }
}

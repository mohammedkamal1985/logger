﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Logging.Infrastructure.Interfaces
{
    public interface ILogger
    {
        Task FatalAsync(string domainName, string message, Exception ex, string contextualData = "");
        Task ErrorAsync(string domainName, string message, Exception ex, string contextualData = "");
        Task WarnAsync(string domainName, string message, string contextualData = "");
        Task InfoAsync(string domainName, string message, string contextualData = "");
        Task DebugAsync(string domainName, string message, string contextualData = "");
        Task TraceAsync(string domainName, string message, string contextualData = "");
        Task LogAsync(LogLevels logLevel, string domainName, string message, string contextualData = "",Exception ex =null);
    }
}

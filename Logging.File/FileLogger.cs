﻿using Logging.Infrastructure;
using Logging.Infrastructure.Interfaces;
using System;
using System.Threading.Tasks;

namespace Logging.File
{
    public class FileLogger : ILogger
    {
        private NLog.ILogger _fileLogger;
        private const string DOMAIN_NAME = "domain";
        private const string CONTEXTULAL_DATA = "extradata";
        public FileLogger(string minimumLogLevel)
        {
            AddLoggingRule(minimumLogLevel);
            _fileLogger = NLog.LogManager.GetCurrentClassLogger();
        }

        private void AddLoggingRule(string minimumLogLevel)
        {
            var level = NLogHelper.GetLogLevel(Enum.Parse<LogLevels>(minimumLogLevel));
            var target = NLog.LogManager.Configuration.AllTargets[0];
            NLog.LogManager.Configuration.LoggingRules.Add(new NLog.Config.LoggingRule("*", level, target));
        }

        public Task DebugAsync(string domainName, string message, string contextualData = "")
        {
            return LogAsync(LogLevels.Debug, domainName, message, contextualData);
        }

        public Task ErrorAsync(string domainName, string message, Exception ex, string contextualData = "")
        {
            return LogAsync(LogLevels.Error, domainName, message, contextualData,ex);
        }

        public Task FatalAsync(string domainName, string message, Exception ex, string contextualData = "")
        {
            return LogAsync(LogLevels.Fatal, domainName, message, contextualData,ex);
        }

        public Task InfoAsync(string domainName, string message, string contextualData = "")
        {
            return LogAsync(LogLevels.Info, domainName, message, contextualData);
        }

        public Task LogAsync(LogLevels logLevel, string domainName, string message, string contextualData = "", Exception ex = null)
        {
            NLog.LogManager.Configuration.Variables[DOMAIN_NAME] = domainName;
            NLog.LogManager.Configuration.Variables[CONTEXTULAL_DATA] = contextualData;
            _fileLogger.Log(NLogHelper.GetLogLevel(logLevel), ex, message);

            return Task.FromResult(0);
        }

        public Task TraceAsync(string domainName, string message, string contextualData = "")
        {
            return LogAsync(LogLevels.Trace, domainName, message, contextualData);
        }

        public Task WarnAsync(string domainName, string message, string contextualData = "")
        {
            return LogAsync(LogLevels.Warn, domainName, message, contextualData);
        }
    }
}

﻿using Logging.Database.Repository.Entity;
using System.Threading.Tasks;

namespace Logging.Database.Repository.Interface
{
    public interface ILogRepository
    {
        Task Save(LogEntity log);
    }
}

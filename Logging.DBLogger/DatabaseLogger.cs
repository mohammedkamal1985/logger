﻿using System;
using System.Threading.Tasks;
using Logging.Database.Repository.Concrete;
using Logging.Database.Repository.Entity;
using Logging.Database.Repository.Interface;
using Logging.Infrastructure;
using Logging.Infrastructure.Interfaces;

namespace Logging.Database
{
    public class DatabaseLogger : ILogger
    {
        private ILogRepository _loggerRepository;
        private LogLevels _minimumLogLevel;

        public DatabaseLogger(string serverType, string connectionString,string minimumLogLevel)
        {
            _loggerRepository = RepositoryFactory.CreateRepository(serverType, connectionString);
            _minimumLogLevel = Enum.Parse<LogLevels>(minimumLogLevel);
        }

        public Task DebugAsync(string domainName, string message, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Debug, domainName, message, contextualData);
        }

        public Task ErrorAsync(string domainName, string message, Exception ex, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Error, domainName, message, contextualData, ex);
        }

        public Task FatalAsync(string domainName, string message, Exception ex, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Fatal, domainName, message, contextualData, ex);
        }

        public Task InfoAsync(string domainName, string message, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Info, domainName, message, contextualData);
        }

        public Task LogAsync(LogLevels logLevel, string domainName, string message, string contextualData = "", Exception ex = null)
        {
            if(logLevel<_minimumLogLevel)
            {
                return null;
            }
            #region Create Log Entity
            var log = CreateLogEntity(logLevel, domainName, message, contextualData, ex);
            #endregion

            return _loggerRepository.Save(log);
        }

        public Task TraceAsync(string domainName, string message, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Trace, domainName, message, contextualData);
        }

        public Task WarnAsync(string domainName, string message, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Warn, domainName, message, contextualData);
        }

        private LogEntity CreateLogEntity(LogLevels logLevel, string domainName, string message, string contextualData = "", Exception ex = null)
        {
            return new LogEntity
            {
                DomainName = domainName,
                ExceptionMessage = ex == null ? string.Empty : ex.Message,
                ExtraData = contextualData,
                Level = logLevel.ToString(),
                Message = message,
                StackTrace = ex == null ? string.Empty : ex.StackTrace,
                TimeStamp = DateTime.Now
            };
        }
    }
}

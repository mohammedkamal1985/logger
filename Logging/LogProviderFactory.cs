﻿using Logging.Database;
using Logging.File;
using Logging.Infrastructure.Entity;
using Logging.Infrastructure.Interfaces;
using Logging.Resources;

namespace Logging
{
    public class LogProviderFactory
    {
        public static ILogger CreateLogger(Target target)
        {
            switch(target.Type)
            {
                case Constants.LOG_TARGET_TYPE_DATABASE:
                     return new DatabaseLogger(target.Server, target.ConnectionString,target.MinimumLogLevel);
                case Constants.LOG_TARGET_TYPE_FILE:
                    return new FileLogger(target.MinimumLogLevel);
                default:
                    return null;
            }
        }
    }
}

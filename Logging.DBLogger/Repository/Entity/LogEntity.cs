﻿using System;

namespace Logging.Database.Repository.Entity
{
    public class LogEntity
    {
        public DateTime TimeStamp { get; set; }
        public string DomainName { get; set; }
        public string Level { get; set; }
        public string ExtraData { get; set; }
        public string Message { get; set; }
        public string ExceptionMessage { get; set; }
        public string StackTrace { get; set; }
    }
}

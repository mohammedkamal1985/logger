﻿using System;

namespace Logging.Resources
{
    public class Constants
    {
        public const string LOG_TARGET_TYPE_FILE = "file";
        public const string LOG_TARGET_TYPE_DATABASE = "database";
        public const string LOG_TARGET_TYPE_CONSOLE = "console";

        public const string LOG_DATABASE_SERVER_SQL = "sql";
        public const string LOG_DATABASE_SERVER_MONGO = "mongo";

        public const string LOQ_SQL_STORED_PROCEDURE_NAME = "LogsInsert";
    }
}

﻿using Logging.Database.Repository.Interface;
using Logging.Resources;

namespace Logging.Database.Repository.Concrete
{
    public class RepositoryFactory
    {
        public static ILogRepository CreateRepository(string serverType, string connectionString)
        {
            switch(serverType)
            {
                case Constants.LOG_DATABASE_SERVER_SQL:
                    return new SqlLoggerRepository(connectionString);
                default:
                    return null;
            }
        }
    }
}

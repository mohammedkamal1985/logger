﻿using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using Logging.Resources;
using Logging.Database.Repository.Entity;
using Logging.Database.Repository.Interface;

namespace Logging.Database.Repository.Concrete
{
    public class SqlLoggerRepository : ILogRepository
    {
        private IDbConnection _connection;

        public SqlLoggerRepository()
        {

        }

        public SqlLoggerRepository(string connectionString)
        {
            this._connection = new SqlConnection(connectionString);
        }

        public Task Save(LogEntity log)
        {
            _connection.Open();
            var parameters = new DynamicParameters();
            parameters.Add("TimeStamp", log.TimeStamp);
            parameters.Add("LogLevel", log.Level);
            parameters.Add("DomainName", log.DomainName);
            parameters.Add("Message", log.Message);
            parameters.Add("ExtraData", log.ExtraData);
            parameters.Add("Exception", log.ExceptionMessage);
            parameters.Add("StackTrace", log.StackTrace);

            var result = _connection.ExecuteAsync(Constants.LOQ_SQL_STORED_PROCEDURE_NAME, parameters, null, null, CommandType.StoredProcedure);
            _connection.Close();
            return result;
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace LoggingTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Logging.Infrastructure.Interfaces.ILogger logger = new Logging.Logger();
            string domain = "TEST_DOMAIN";
            string message = "Hello World!";
            for (int i = 0; i < 1000; i++)
            {
                try
                {
                    int x = int.Parse("sadas");
                }
                catch (Exception ex)
                {
                    logger.ErrorAsync(domain, message, ex);
                    logger.FatalAsync(domain, message, ex);
                }
                Console.WriteLine(message);
                logger.DebugAsync(domain, message);
                logger.InfoAsync(domain, message);
                logger.TraceAsync(domain, message);
                logger.WarnAsync(domain, message);
                
            }
            Console.ReadLine();
        }


    }
}

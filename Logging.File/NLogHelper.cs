﻿using Logging.Infrastructure;

namespace Logging.File
{
    public class NLogHelper
    {
        public static NLog.LogLevel GetLogLevel(LogLevels level)
        {
            switch (level)
            {
                case LogLevels.Debug:
                    return NLog.LogLevel.Debug;
                case LogLevels.Error:
                    return NLog.LogLevel.Error;
                case LogLevels.Fatal:
                    return NLog.LogLevel.Fatal;
                case LogLevels.Info:
                    return NLog.LogLevel.Info;
                case LogLevels.Trace:
                    return NLog.LogLevel.Trace;
                case LogLevels.Warn:
                    return NLog.LogLevel.Warn;
                case LogLevels.All:
                    return NLog.LogLevel.Trace;
                default:
                    return NLog.LogLevel.Trace;
            }
        }
    }
}

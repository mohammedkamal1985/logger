﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Logging.Infrastructure;
using Logging.Infrastructure.Entity;
using Logging.Infrastructure.Interfaces;

namespace Logging
{
    public class Logger : ILogger
    {
         private readonly IList<ILogger> _loggers = new List<ILogger>();

        private IConfiguration GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
               .AddJsonFile("logsettings.json");

            return builder.Build();
        }

        private LogSettings GetLogSettings(IConfiguration configuration)
        {
            var settings = new LogSettings();
            ConfigurationBinder.Bind(configuration.GetSection("LogSettings"), settings);
            return settings;
        }

        public Logger()
        {
            var settings = GetLogSettings(GetConfiguration());
            foreach(var target in settings.Targets)
            {
                var logProvider = LogProviderFactory.CreateLogger(target);
                if(logProvider != null)
                {
                    _loggers.Add(logProvider);
                }
            }
        }

        #region LOG FUNCTIONS
        public Task DebugAsync(string domainName, string message, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Debug, domainName, message, contextualData);
        }

        public Task ErrorAsync(string domainName, string message, Exception ex, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Error, domainName, message, contextualData, ex);
        }

        public Task FatalAsync(string domainName, string message, Exception ex, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Fatal, domainName, message, contextualData, ex);
        }

        public Task InfoAsync(string domainName, string message, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Info, domainName, message, contextualData);
        }

        public Task LogAsync(LogLevels logLevel, string domainName, string message, string contextualData = "", Exception ex = null)
        {
            if (_loggers == null)
                return null;
            List<Exception> loggingExceptions = null;
            foreach (var logger in _loggers)
            {
                try
                {
                    logger.LogAsync(logLevel, domainName, message, contextualData, ex);
                }
                catch (Exception logException)
                {
                    if (loggingExceptions == null)
                        loggingExceptions = new List<Exception>();

                    loggingExceptions.Add(logException);
                }
                if (loggingExceptions != null && loggingExceptions.Count > 0)
                {
                    throw new LogCreationException("Error occured while creating log(s).", loggingExceptions);
                }
            }
            return Task.FromResult(0);
        }

        public Task TraceAsync(string domainName, string message, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Trace, domainName, message, contextualData);
        }

        public Task WarnAsync(string domainName, string message, string contextualData = "")
        {
            return this.LogAsync(LogLevels.Warn, domainName, message, contextualData);
        }
        #endregion LOG FUNCTIONS
    }
}

﻿using System;
using System.Collections.Generic;

namespace Logging.Infrastructure
{
    public class LogCreationException:AggregateException
    {
        public LogCreationException(string message, List<Exception> exceptions) : base(message, exceptions)
        {

        }
    }

}
